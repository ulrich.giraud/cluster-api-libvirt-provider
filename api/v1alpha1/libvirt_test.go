package v1alpha1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConnectionStringTestDriver(t *testing.T) {

	connection := &LibvirtConnection{
		Driver: "test",
		Path:   "/default",
	}

	assert.Equal(t, "test:///default", connection.ConnectionString())
}

func TestConnectionStringDriverProtocolHost(t *testing.T) {

	connection := &LibvirtConnection{
		Driver:   "qemu",
		Protocol: "ssh",
		Hostname: "testhost",
	}

	assert.Equal(t, "qemu+ssh://testhost", connection.ConnectionString())
}

func TestConnectionStringAllParameters(t *testing.T) {

	connection := &LibvirtConnection{
		Driver:   "qemu",
		Protocol: "ssh",
		Hostname: "testhost",
		Username: "testuser",
		Port:     2222,
		Path:     "/testpath",
		ExtraParameters: map[string]string{
			"param1": "value1",
		},
	}

	assert.Equal(t, "qemu+ssh://testuser@testhost:2222/testpath?param1=value1", connection.ConnectionString())
}

func TestConnectionStringTwoExtraParams(t *testing.T) {

	connection := &LibvirtConnection{
		Driver:   "qemu",
		Protocol: "ssh",
		Hostname: "testhost",
		Username: "testuser",
		Port:     2222,
		Path:     "/testpath",
		ExtraParameters: map[string]string{
			"param1": "value1",
			"param2": "value2",
		},
	}

	assert.Contains(t, connection.ConnectionString(), "param1=value1")
	assert.Contains(t, connection.ConnectionString(), "param2=value2")
	assert.Contains(t, connection.ConnectionString(), "&param")
}
