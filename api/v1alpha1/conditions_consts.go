package v1alpha1

import clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"

const (
	// LibvirtAvailableCondition documents the connectivity with libvirt
	// for a given resource.
	LibvirtAvailableCondition clusterv1.ConditionType = "LibvirtAvailable"

	// LibvirtUnreachableReason (Severity=Error) documents a controller detecting
	// issues with libvirt reachability.
	LibvirtUnreachableReason = "LibvirtUnreachable"
)
