package util

import (
	"os"
	"path/filepath"
)

var (
	osHomeDir = os.UserHomeDir
)

type SSHKey struct {
	Key  string
	Name string
}

func (s *SSHKey) CreatePrivateKey() (err error) {
	keyPath, _ := filepath.Split(s.Path())
	err = os.MkdirAll(keyPath, os.FileMode(0700))
	if err != nil {
		return
	}

	err = CreateOrUpdateFile(s.Key, os.FileMode(0600), s.Path())
	return
}

func (s *SSHKey) Path() (path string) {
	home, _ := osHomeDir()
	path = home + "/.ssh/" + s.Name

	return
}
