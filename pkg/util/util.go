package util

import (
	"crypto/sha256"
	"errors"
	"io/fs"
	"os"
)

func CreateOrUpdateFile(content string, mode os.FileMode, path string) (err error) {
	fc, err := os.ReadFile(path)
	if !errors.Is(err, fs.ErrNotExist) && sha256.Sum256([]byte(content)) == sha256.Sum256(fc) {
		os.Chmod(path, mode)
		return nil
	}

	f, err := os.Create(path)
	if err != nil {
		return
	}
	defer f.Close()

	_, err = f.WriteString(content)
	if err != nil {
		return
	}
	f.Sync()
	os.Chmod(path, mode)

	return
}
