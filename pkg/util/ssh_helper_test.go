package util

import (
	"os"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreatePrivateKey(t *testing.T) {
	dir, err := os.MkdirTemp("", "util_test")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(dir)

	key := SSHKey{
		Key:  "test",
		Name: "test",
	}

	// Save current function and restore at the end:
	old := osHomeDir
	defer func() { osHomeDir = old }()
	osHomeDir = func() (string, error) {
		return dir, nil
	}

	err = key.CreatePrivateKey()
	assert.Nil(t, err)

	f, err := os.Open(key.Path())
	assert.Nil(t, err, "Failed to open created file")

	stat, err := f.Stat()
	assert.Nil(t, err, "Failed to get file stat")

	assert.Equal(t, os.FileMode(0600), stat.Mode().Perm())
	assert.Contains(t, key.Path(), "/.ssh/")
}

func TestCreatePrivateKeyInvalidPath(t *testing.T) {
	dir := "/tmp/golang\000"
	defer os.RemoveAll(dir)

	key := SSHKey{
		Key:  "test",
		Name: "test",
	}

	// Save current function and restore at the end:
	old := osHomeDir
	defer func() { osHomeDir = old }()
	osHomeDir = func() (string, error) {
		return dir, nil
	}

	err := key.CreatePrivateKey()
	assert.Error(t, err)
	assert.Equal(t, "*fs.PathError", reflect.TypeOf(err).String())
}
