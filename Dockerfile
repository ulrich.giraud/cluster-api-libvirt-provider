# Build the manager binary
FROM golang:1.17 as builder

WORKDIR /workspace

RUN apt-get update && apt-get install -y libvirt-dev
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy the go source
COPY main.go main.go
COPY api/ api/
COPY controllers/ controllers/
COPY pkg/ pkg/

# Build
RUN GOOS=linux GOARCH=amd64 go build -a -o manager main.go


# FROM debian:stretch
# RUN apt-get update && apt-get install -y libvirt-dev && rm -rf /var/lib/apt/lists/*

FROM docker.io/fedora:35
RUN INSTALL_PKGS=" \
      libvirt-libs openssh-clients genisoimage \
      " && \
    yum install -y $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all
RUN groupadd -g 35532 libvirt &&  useradd -m -u 35532 -g 35532 libvirt
WORKDIR /
COPY --from=builder /workspace/manager /
USER 35532:35532

ENTRYPOINT ["/manager"]
