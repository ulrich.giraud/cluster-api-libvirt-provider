/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
	"sigs.k8s.io/cluster-api/util/conditions"
	"sigs.k8s.io/cluster-api/util/patch"
	"sigs.k8s.io/controller-runtime/pkg/client"

	infrastructurev1alpha1 "gitlab.com/ulrich.giraud/cluster-api-provider-libvirt/api/v1alpha1"
)

// +kubebuilder:docs-gen:collapse=Imports

var _ = Describe("Libvirt CLuster controller", func() {
	BeforeEach(func() {})
	AfterEach(func() {})

	// Define utility constants for object names and testing timeouts/durations and intervals.
	const (
		ClusterName      = "test-cluster"
		ClusterNamespace = "default"

		timeout  = time.Second * 10
		duration = time.Second * 10
		interval = time.Millisecond * 250
	)

	Context("Reconcile a cluster", func() {
		It("should create a cluster", func() {
			instance := &infrastructurev1alpha1.LibvirtCluster{
				ObjectMeta: metav1.ObjectMeta{
					GenerateName: ClusterName,
					Namespace:    ClusterNamespace,
				},
				Spec: infrastructurev1alpha1.LibvirtClusterSpec{
					LibvirtConnection: infrastructurev1alpha1.LibvirtConnection{
						Driver: "test",
						Path:   "/default",
					},
				},
			}

			Expect(k8sClient.Create(ctx, instance)).To(Succeed())
			key := client.ObjectKey{Namespace: instance.Namespace, Name: instance.Name}
			defer func() {
				Expect(k8sClient.Delete(ctx, instance)).To(Succeed())
			}()

			capiCluster := &clusterv1.Cluster{
				ObjectMeta: metav1.ObjectMeta{
					GenerateName: "test1-",
					Namespace:    ClusterNamespace,
				},
				Spec: clusterv1.ClusterSpec{
					InfrastructureRef: &corev1.ObjectReference{
						APIVersion: infrastructurev1alpha1.GroupVersion.String(),
						Kind:       "LibvirtCluster",
						Name:       instance.Name,
					},
				},
			}
			// Create the CAPI cluster (owner) object
			Expect(k8sClient.Create(ctx, capiCluster)).To(Succeed())
			defer func() {
				Expect(k8sClient.Delete(ctx, capiCluster)).To(Succeed())
			}()

			// Make sure the LibvirtCluster exists.
			Eventually(func() error {
				return k8sClient.Get(ctx, key, instance)
			}, timeout).Should(BeNil())

			By("setting the OwnerRef on the LibvirtCluster")
			Eventually(func() error {
				ph, err := patch.NewHelper(instance, k8sClient)
				Expect(err).ShouldNot(HaveOccurred())
				instance.OwnerReferences = append(instance.OwnerReferences, metav1.OwnerReference{
					Kind:       "Cluster",
					APIVersion: clusterv1.GroupVersion.String(),
					Name:       capiCluster.Name,
					UID:        "blah",
				})
				return ph.Patch(ctx, instance, patch.WithStatusObservedGeneration{})
			}, timeout).Should(BeNil())

			Eventually(func() bool {
				if err := k8sClient.Get(ctx, key, instance); err != nil {
					return false
				}
				return len(instance.Finalizers) > 0
			}, timeout).Should(BeTrue())

			By("setting the LibvirtCluster's LibvirtAvailableCondition to true")
			Eventually(func() bool {
				if err := k8sClient.Get(ctx, key, instance); err != nil {
					return false
				}
				return conditions.IsTrue(instance, infrastructurev1alpha1.LibvirtAvailableCondition)
			}, timeout).Should(BeTrue())
		})
	})
})

/*
	After writing all this code, you can run `go test ./...` in your `controllers/` directory again to run your new test!
*/
