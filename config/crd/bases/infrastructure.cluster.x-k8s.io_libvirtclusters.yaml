---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.8.0
  creationTimestamp: null
  name: libvirtclusters.infrastructure.cluster.x-k8s.io
spec:
  group: infrastructure.cluster.x-k8s.io
  names:
    kind: LibvirtCluster
    listKind: LibvirtClusterList
    plural: libvirtclusters
    singular: libvirtcluster
  scope: Namespaced
  versions:
  - additionalPrinterColumns:
    - description: Cluster infrastructure is ready for libvirtMachine
      jsonPath: .status.ready
      name: Ready
      type: string
    - description: Hostname is the address of the libvirt endpoint.
      jsonPath: .spec.libvirtConnection.Hostname
      name: Host
      type: string
    - description: API Endpoint
      jsonPath: .spec.controlPlaneEndpoint[0]
      name: ControlPlaneEndpoint
      priority: 1
      type: string
    name: v1alpha1
    schema:
      openAPIV3Schema:
        description: LibvirtCluster is the Schema for the libvirtclusters API
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: LibvirtClusterSpec defines the desired state of LibvirtCluster
            properties:
              controlPlaneEndpoint:
                description: ControlPlaneEndpoint represents the endpoint used to
                  communicate with the control plane.
                properties:
                  host:
                    description: The hostname on which the API server is serving.
                    type: string
                  port:
                    default: 6443
                    description: The port on which the API server is serving.
                    format: int32
                    type: integer
                required:
                - host
                - port
                type: object
              libvirtConnection:
                description: LibvirtConnection contains parameters to access a libvirt
                  daemon
                properties:
                  driver:
                    description: Libvirt driver used on the host. See https://libvirt.org/uri.html
                      for details
                    enum:
                    - qemu
                    - test
                    type: string
                  extraParameters:
                    additionalProperties:
                      type: string
                    description: ExtraParameters for the connection. See https://libvirt.org/uri.html
                      for details
                    type: object
                  hostname:
                    description: Hostname to connect, may be system, session or a
                      distant host. See https://libvirt.org/uri.html for details
                    type: string
                  path:
                    description: Path is the remote host absolute path context. See
                      https://libvirt.org/uri.html for details
                    type: string
                  port:
                    description: Port is the remote port listening for the protocol
                      used. See https://libvirt.org/uri.html for details
                    maximum: 65535
                    minimum: 1
                    type: integer
                  protocol:
                    description: Protocol to use to connect to the remote host. See
                      https://libvirt.org/uri.html for details
                    enum:
                    - ssh
                    - libssh
                    - libssh2
                    - tcp
                    type: string
                  sshPrivateKeyRef:
                    description: 'SSHPrivateKeyRef is a reference to a ssh private
                      key in a Kubernetes Secret. key path for keyfile ExtraParameter
                      will be : /home/libvirt/.ssh/<cluster-name>-<SSHPrivateKeyRef.key>-id'
                    properties:
                      key:
                        description: Key is the key within the secret referenced
                        type: string
                      name:
                        description: Name is the name of the Kubernetes Secret
                        type: string
                    required:
                    - key
                    - name
                    type: object
                  username:
                    description: Username to use for remote host connection. See https://libvirt.org/uri.html
                      for details
                    type: string
                required:
                - driver
                - path
                type: object
            required:
            - libvirtConnection
            type: object
          status:
            description: LibvirtClusterStatus defines the observed state of LibvirtCluster
            properties:
              conditions:
                description: Conditions defines current service state of the LibvirtCluster.
                items:
                  description: Condition defines an observation of a Cluster API resource
                    operational state.
                  properties:
                    lastTransitionTime:
                      description: Last time the condition transitioned from one status
                        to another. This should be when the underlying condition changed.
                        If that is not known, then using the time when the API field
                        changed is acceptable.
                      format: date-time
                      type: string
                    message:
                      description: A human readable message indicating details about
                        the transition. This field may be empty.
                      type: string
                    reason:
                      description: The reason for the condition's last transition
                        in CamelCase. The specific API may choose whether or not this
                        field is considered a guaranteed API. This field may not be
                        empty.
                      type: string
                    severity:
                      description: Severity provides an explicit classification of
                        Reason code, so the users or machines can immediately understand
                        the current situation and act accordingly. The Severity field
                        MUST be set only when Status=False.
                      type: string
                    status:
                      description: Status of the condition, one of True, False, Unknown.
                      type: string
                    type:
                      description: Type of condition in CamelCase or in foo.example.com/CamelCase.
                        Many .condition.type values are consistent across resources
                        like Available, but because arbitrary conditions can be useful
                        (see .node.status.conditions), the ability to deconflict is
                        important.
                      type: string
                  required:
                  - lastTransitionTime
                  - status
                  - type
                  type: object
                type: array
              failureDomains:
                additionalProperties:
                  description: FailureDomainSpec is the Schema for Cluster API failure
                    domains. It allows controllers to understand how many failure
                    domains a cluster can optionally span across.
                  properties:
                    attributes:
                      additionalProperties:
                        type: string
                      description: Attributes is a free form map of attributes an
                        infrastructure provider might use or require.
                      type: object
                    controlPlane:
                      description: ControlPlane determines if this failure domain
                        is suitable for use by control plane machines.
                      type: boolean
                  type: object
                description: FailureDomains is a list of failure domain objects synced
                  from the infrastructure provider.
                type: object
              ready:
                type: boolean
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
