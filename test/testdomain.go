package main

import (
	"fmt"

	libvirt "libvirt.org/libvirt-go"
)

func main() {
	// cnxStr := "qemu+tcp://localhost:16509/system"
	cnxStr := "qemu+tcp://10.0.4.126:16509/system"
	// cnxStr := "qemu+tcp:///system"
	fmt.Println("Connecting to " + cnxStr)
	conn, err := libvirt.NewConnect(cnxStr)
	if err != nil {
		fmt.Println("failed to connect")
	}
	defer conn.Close()

	doms, err := conn.ListAllDomains(libvirt.CONNECT_LIST_DOMAINS_PERSISTENT)
	if err != nil {
		fmt.Println("failed to list")
	}

	fmt.Printf("%d active domains:\n", len(doms))
	for _, dom := range doms {
		name, err := dom.GetName()
		if err == nil {
			fmt.Printf("  %s\n", name)
		}
		dom.Free()
	}
}
